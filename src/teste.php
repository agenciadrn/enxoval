<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, Authorization, Accept, X-Requested-With, x-xsrf-token');
header('Content-Type: application/json; charset=utf-8');

include "config.php";

$postjson = json_decode(file_get_contents('php://input'), true);
date_default_timezone_set('America/Sao_Paulo');




if ($postjson['aksi'] == "registrar_mov") {


        $insert = mysqli_query($mysqli, "INSERT INTO movimentacoes SET
           
            valor_m              = '$postjson[valor_m]',
            valor2               = '$postjson[valor2]',
            opcao1               = '$postjson[opcao1]',
            opcao2               = '$postjson[opcao2]',
            idcliente_m          = '$postjson[idcliente_m]',
            detalhes_m           = '$postjson[detalhes_m]',
            categoria_m          = '$postjson[categoria_m]',
            funcionario          = '$postjson[funcionario]',
            rota                 = '$postjson[rota]',
            dia_m                 = '$postjson[dia_m]'
            
    
        ");
        if ($insert) {
            $result = json_encode(array('success' => true, 'msg' => 'Movimentação registrada'));
        } else {
            $result = json_encode(array('success' => false, 'msg' => 'Erro ao registrar movimentação'));
        }
  
    echo $result;
    
}  elseif ($postjson['aksi'] == "load_mov") {
    $data = array();

    $query = mysqli_query($mysqli, "SELECT *, DATE_FORMAT(dia_m, 'Registrado em %d/%m/%Y ás %H:%i') AS dia_m FROM movimentacoes ORDER BY dia_m DESC LIMIT $postjson[start],$postjson[limit]");

    while ($rows = mysqli_fetch_array($query)) {
        $data[] = array(
            'id_movimentacao'            => $rows['id_movimentacao'],
            'valor_m'              => str_replace('.', ',', $rows['valor_m']),
            'valor2'                  => str_replace('.', ',', $rows['valor2']),
            'opcao1'               => $rows['opcao1'],
            'opcao2'             => $rows['opcao2'],
            'idcliente_m'               => $rows['idcliente_m'],
            'categoria_m'               => $rows['categoria_m'],
            'detalhes_m'          => $rows['detalhes_m'],
            'dia_m'          => $rows['dia_m'],
            'funcionario'               => $rows['funcionario'],
            'rota'               => $rows['rota']
        );
    }

    if ($query) {
        $result = json_encode(array('success' => true, 'result' => $data));
    } else {
        $result = json_encode(array('success' => false));
    }

    echo $result;
} elseif ($postjson['aksi'] == "del_mov") {

    $query = mysqli_query($mysqli, "DELETE FROM movimentacoes WHERE id_movimentacao='$postjson[id]'");

    if ($query) {
        $result = json_encode(array('success' => true));
    } else {
        $result = json_encode(array('success' => false));
    }

    echo $result;
} elseif ($postjson['aksi'] == "crud_mov") {


    if ($postjson['action']=="Cadastrado"){

        $insert = mysqli_query($mysqli, "INSERT INTO movimentacoes SET
            valor_m            = '$postjson[valor_m]',
            valor2             = '$postjson[valor2]',
            opcao1             = '$postjson[opcao1]',
            opcao2          = '$postjson[opcao2]',
            categoria_m          = '$postjson[categoria_m]',
            idcliente_m        = '$postjson[idcliente_m]',
            detalhes_m          = '$postjson[detalhes_m]',
            funcionario          = '$postjson[funcionario]',
            rota          = '$postjson[rota]',
            dia_m          = '$postjson[dia_m]'
    
        ");
        if ($insert) {
            $result = json_encode(array('success' => true, 'msg' => ' com sucesso'));
        } else {
            $result = json_encode(array('success' => false, 'msg' => ' com erro'));
        }
}else{

    $updt = mysqli_query($mysqli, "UPDATE movimentacoes SET
            valor2             = '$postjson[valor2]',
            opcao1          = '$postjson[opcao1]',
            categoria_m          = '$postjson[categoria_m]',
            opcao2        = '$postjson[opcao2]',
            idcliente_m          = '$postjson[idcliente_m]',
            detalhes_m          = '$postjson[detalhes_m]',
            funcionario          = '$postjson[funcionario]',
            rota          = '$postjson[rota]',
            dia_m          = '$postjson[dia_m]',
            valor_m             = '$postjson[valor_m]' WHERE id_movimentacao='$postjson[id]'
    
        ");
        if ($updt) {
            $result = json_encode(array('success' => true, 'msg' => ' com sucesso'));
        } else {
            $result = json_encode(array('success' => false, 'msg' => ' com erro'));
        }



}
    echo $result;
    
}elseif ($postjson['aksi'] == "single_mov") {
   

    $query = mysqli_query($mysqli, "SELECT * FROM movimentacoes WHERE id_movimentacao='$postjson[id]'");

    while ($rows = mysqli_fetch_array($query)) {

        $data = array(
            'valor_m'          => str_replace('.', ',', $rows['valor_m']),
            'valor2'             => str_replace('.', ',', $rows['valor2']),
            'opcao1'      => $rows['opcao1'],
            'opcao2'      => $rows['opcao2'],
            'categoria_m'  => $rows['categoria_m'],
            'idcliente_m'      => $rows['idcliente_m'],
            'detalhes_m'      => $rows['detalhes_m'],
            'funcionario'      => $rows['funcionario'],
            'rota'      => $rows['rota'],
            'dia_m'      => $rows['dia_m'],
        );
    }

    if ($query) {
        $result = json_encode(array('success' => true, 'result' => $data));
    } else {
        $result = json_encode(array('success' => false));
    }

    echo $result;
}elseif ($postjson['aksi'] == "busca_mov") {
    $data = array();

    $query = mysqli_query($mysqli, "SELECT *, DATE_FORMAT(dia_m, 'Registrado em %d/%m/%Y ás %H:%i') AS dia_m FROM movimentacoes WHERE dia_m >= (CURDATE() - INTERVAL $postjson[id] DAY ) ORDER BY dia_m DESC LIMIT $postjson[start],$postjson[limit]");

    while ($rows = mysqli_fetch_array($query)) {
        $data[] = array(
            'id_movimentacao'            => $rows['id_movimentacao'],
            'valor_m'              => $rows['valor_m'],
            'valor2'                  => $rows['valor2'],
            'opcao1'               => $rows['opcao1'],
            'opcao2'             => $rows['opcao2'],
            'idcliente_m'               => $rows['idcliente_m'],
            'categoria_m'               => $rows['categoria_m'],
            'detalhes_m'          => $rows['detalhes_m'],
            'dia_m'          => $rows['dia_m'],
            'funcionario'               => $rows['funcionario'],
            'rota'               => $rows['rota']
        );
    }

    if ($query) {
        $result = json_encode(array('success' => true, 'result' => $data));
    } else {
        $result = json_encode(array('success' => false));
    }

    echo $result;
}
