import { Injectable } from '@angular/core';
import { ToastController, LoadingController, AlertController, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { File } from "@ionic-native/file/ngx";
import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer/ngx";
import { FileOpener } from "@ionic-native/file-opener/ngx";


@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  
  fileTransfer: FileTransferObject;

  constructor(
    public NavCtrl: NavController,
    public toastCtrl: ToastController,
    private storage: Storage,
    private fileOpener: FileOpener,
    private transfer: FileTransfer,
    private file: File
  ) { }

  download(url: string, title: string) {
    this.fileTransfer = this.transfer.create();
    this.fileTransfer
      .download(url, this.file.dataDirectory + title + ".xls")
      .then(entry => {
        console.log("download concluido: " + entry.toURL());
        this.fileOpener
          .open(entry.toURL(), "application/excel")
          .then(() => console.log("Arquivo aberto"))
          .catch(e => console.log("Erro ao abrir", e));
      });
  }

  efeitoLayout(){
    this.animateCSS('tab', 'bounceOutLeft', function handleAnimationEnd() {
    })
    this.animateCSS('topo', 'bounceOutRight', function handleAnimationEnd() {
    })
    this.animateCSS('conteudo', 'bounceOutRight', function handleAnimationEnd() {
    })
    
  }

  

  animateCSS(element, animationName, callback) {
    const node = document.getElementById(element)
    node.classList.add('animated', animationName)

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName)
        node.removeEventListener('animationend', handleAnimationEnd)

        if (typeof callback === 'function') callback()
    }

    node.addEventListener('animationend', handleAnimationEnd)
}

addMov(){
  this.efeitoLayout();
  this.NavCtrl.navigateForward(['/crud-mov/0']);
}

linkControleLayout(){
  this.efeitoLayout();
  this.NavCtrl.navigateForward(['/controle/']);
}

linkControle(){
  this.NavCtrl.navigateForward(['/controle/']);
}
linkLayout(){
  this.NavCtrl.navigateForward(['/layout/']);
}
linkVoltar(){
  this.NavCtrl.navigateBack(['/controle/']);
}
linkProdutos(){
  this.NavCtrl.navigateForward(['/produtos/']);
}
linkCategorias(){
  this.NavCtrl.navigateForward(['/categorias/']);
}
linkExtracoes(a){
  this.NavCtrl.navigateForward(['/extracoes/' + a]);
}
linkClientes(){
  this.NavCtrl.navigateForward(['/clientes/']);
}
linkFuncionarios(){
  this.NavCtrl.navigateForward(['/funcionarios/']);
}
linkSobre(){
  this.NavCtrl.navigateForward(['/sobre/']);
}

linkMov(){
  this.NavCtrl.navigateForward(['/mov/']);
}

linkRelatorios(){
  this.NavCtrl.navigateForward(['/relatorios/']);
}

linkEdClientes(a){
  this.NavCtrl.navigateForward(['/crud-clientes/' + a]);
}

linkEdCategorias(a){
  this.NavCtrl.navigateForward(['/crud-categorias/' + a]);
}

linkEdFunc(a){
  this.NavCtrl.navigateForward(['/crud-func/' + a]);
}

linkBuscar(a){
  this.NavCtrl.navigateForward(['/buscar/' + a]);
}

linkContas(){
  this.NavCtrl.navigateForward('/contas/');
}

async prosesLogout() {
  this.storage.clear();
  this.NavCtrl.navigateRoot(['/intro']);
  const toast = await this.toastCtrl.create({
    message: 'Você saiu',
    duration: 1500
  });
  toast.present();
}

}
 