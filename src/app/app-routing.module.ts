import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'layout', pathMatch: 'full' },
  
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'intro',
    loadChildren: () => import('./pages/intro/intro.module').then( m => m.IntroPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule)
  },
  
  {
    path: 'clientes',
    loadChildren: () => import('./pages/clientes/clientes.module').then( m => m.ClientesPageModule)
  },
  
  {
    path: 'funcionarios',
    loadChildren: () => import('./pages/funcionarios/funcionarios.module').then( m => m.FuncionariosPageModule)
  },
  {
    path: 'mov',
    loadChildren: () => import('./pages/mov/mov.module').then( m => m.MovPageModule)
  },
  
  {
    path: 'crud-func/:id',
    loadChildren: () => import('./pages/crud-func/crud-func.module').then( m => m.CrudFuncPageModule)
  },
  {
    path: 'crud-clientes/:id',
    loadChildren: () => import('./pages/crud-clientes/crud-clientes.module').then( m => m.CrudClientesPageModule)
  },
  {
    path: 'crud-mov/:id',
    loadChildren: () => import('./pages/crud-mov/crud-mov.module').then( m => m.CrudMovPageModule)
  },
  {
    path: 'buscar/:id',
    loadChildren: () => import('./pages/buscar/buscar.module').then( m => m.BuscarPageModule)
  },
  {
    path: 'categorias',
    loadChildren: () => import('./pages/categorias/categorias.module').then( m => m.CategoriasPageModule)
  },
  {
    path: 'crud-categorias/:id',
    loadChildren: () => import('./pages/crud-categorias/crud-categorias.module').then( m => m.CrudCategoriasPageModule)
  },
  {
    path: 'relatorios',
    loadChildren: () => import('./pages/relatorios/relatorios.module').then( m => m.RelatoriosPageModule)
  },
  {
    path: 'layout',
    loadChildren: () => import('./pages/layout/layout.module').then( m => m.LayoutPageModule)
  },
  {
    path: 'controle',
    loadChildren: () => import('./pages/controle/controle.module').then( m => m.ControlePageModule)
  },
  {
    path: 'produtos',
    loadChildren: () => import('./pages/produtos/produtos.module').then( m => m.ProdutosPageModule)
  },
  {
    path: 'sobre',
    loadChildren: () => import('./pages/sobre/sobre.module').then( m => m.SobrePageModule)
  },
  {
    path: 'contas',
    loadChildren: () => import('./pages/contas/contas.module').then( m => m.ContasPageModule)
  },
  {
    path: 'extracoes/:id',
    loadChildren: () => import('./pages/extracoes/extracoes.module').then( m => m.ExtracoesPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
