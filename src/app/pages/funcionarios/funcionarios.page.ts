import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastController, LoadingController, AlertController, NavController, Platform } from '@ionic/angular';
import { AccessProviders } from '../../providers/access-providers';
import { Storage } from '@ionic/storage';
import { GlobalService } from '../../providers/global.service';


@Component({
  selector: 'app-funcionarios',
  templateUrl: './funcionarios.page.html',
  styleUrls: ['./funcionarios.page.scss'],
})
export class FuncionariosPage implements OnInit {
  datastorage: any;
  name: string;

  funcionarios: any = [];
  limit: number = 13;
  start: number = 0;

  constructor(
    private router: Router,
    public global: GlobalService,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    public platform: Platform,
    private accsPrvds: AccessProviders,
    private storage: Storage,
    public NavCtrl: NavController
  ) {

    this.platform.ready().then(()=>{
      this.loadingCtrl.create({
        message: 'Carregando...'
      }).then((loadingElement)=>{
        loadingElement.present();
        var ref = this;
        setTimeout(function()
        {
          ref.loadingCtrl.dismiss();
        }, 2000)
      })
    })

   }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.storage.get('storage_xxx').then((res) => {
      console.log(res);
      this.datastorage = res;
    });

    this.start = 0;
    this.funcionarios = [];
    this.loadFunc();

  }

  async doRefresh(event) {
    const loader = await this.loadingCtrl.create({
      message: 'Aguarde...',
    });
    loader.present();

    this.ionViewDidEnter();
    event.target.complete();

    loader.dismiss();
  }

  loadData(event:any) {
    this.start += this.limit;
    setTimeout(() => {
      this.loadFunc().then(() => {
        event.target.complete();
      });
    }, 500);
  }

  async loadFunc() {

    return new Promise(resolve => {
      let body = {
        aksi: 'load_func',
        start: this.start,
        limit: this.limit
      }
      this.accsPrvds.postData(body, 'funcionarios_api.php').subscribe((res: any) => {
        for (let datas of res.result) {
          this.funcionarios.push(datas);
        }
        resolve(true);
      });
    });
  }

  async delData(a) {

    return new Promise(resolve => {
      let body = {
        aksi: 'del_func',
        id: a
      }
      this.accsPrvds.postData(body, 'funcionarios_api.php').subscribe((res: any) => {
        if (res.success == true) {
          this.presentToast('Deletado com sucesso!');
          this.ionViewDidEnter();
        } else {
          this.presentToast('Erro ao deletar')
        }
      });
    });

  }

  async presentToast(a) {
    const toast = await this.toastCtrl.create({
      message: a,
      duration: 1500
    });
    toast.present();
  }


  openCrud(a) {
    this.router.navigate(['/crud-func/'+a]);
  }

  linkHome() {
    this.router.navigate(['/home']);
  }
}
