import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CrudMovPage } from './crud-mov.page';

const routes: Routes = [
  {
    path: '',
    component: CrudMovPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CrudMovPageRoutingModule {}
