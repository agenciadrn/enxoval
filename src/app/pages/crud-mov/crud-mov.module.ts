import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BrMaskerModule } from 'br-mask';
import { IonicModule } from '@ionic/angular';
import { LottieAnimationViewModule } from 'ng-lottie';

import { CrudMovPageRoutingModule } from './crud-mov-routing.module';

import { CrudMovPage } from './crud-mov.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BrMaskerModule,
    IonicModule,
    CrudMovPageRoutingModule,
    LottieAnimationViewModule
  ],
  declarations: [CrudMovPage]
})
export class CrudMovPageModule {}
