import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CrudMovPage } from './crud-mov.page';

describe('CrudMovPage', () => {
  let component: CrudMovPage;
  let fixture: ComponentFixture<CrudMovPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudMovPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CrudMovPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
