import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastController, LoadingController, AlertController, NavController, Platform } from '@ionic/angular';
import { AccessProviders } from '../../providers/access-providers';
import { LottieAnimationViewModule } from 'ng-lottie';
import { Storage } from '@ionic/storage';
import { GlobalService } from '../../providers/global.service';

@Component({
  selector: 'app-crud-mov',
  templateUrl: './crud-mov.page.html',
  styleUrls: ['./crud-mov.page.scss'],
})
export class CrudMovPage implements OnInit {
  
  datastorage: any;
  id: number;

  valor_m: string = "";
  valor2: string = "";
  opcao1: string = "";
  opcao2: string = "";
  dia_m: string = "";
  idcliente_m: any;
  detalhes_m: string = "";
  categoria_m: string = "";
  funcionario: string = "";
  rota: string = "";
  
  clientes: any = [];
  funcionarios: any = [];
  categorias: any = [];
  
  flag1 = false;
  flag2 = false;
  
  lottieConfig:any;

  disabledButton;

  constructor(
    private router: Router,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private accsPrvds: AccessProviders,
    public platform: Platform,
    private actRoute: ActivatedRoute,
    public NavCtrl: NavController,
    private storage: Storage,
    public global: GlobalService
  ) {

    LottieAnimationViewModule.forRoot();
 
    this.lottieConfig = {
      path: 'assets/img/coin.json',
      autoplay: true,
      loop: true
    }

    this.platform.ready().then(()=>{
      this.loadingCtrl.create({
        message: 'Carregando...'
      }).then((loadingElement)=>{
        loadingElement.present();
        var ref = this;
        setTimeout(function()
        {
          ref.loadingCtrl.dismiss();
        }, 2000)
      })
    })

    this.opcao1 = 'Crédito';
    this.opcao2 = 'Débito';
    this.idcliente_m = 'Sem cliente';
    this.categoria_m = 'Sem categoria';
    this.funcionario = 'Sem funcionário';

    this.loadClientes();
    this.loadFuncionario();
    this.loadCat();
   }

  onExibirR(){
    console.log(this.valor_m);
   if(this.valor_m != undefined){
     this.flag1 = true;
     /// document.getElementById("valor_input").style.display = 'block';
     console.log('true');
    }else{
      this.flag1 = false;
      //document.getElementById("valor_input").style.display = 'block';
      console.log('false');
    }
  }

  onExibirD(){
    console.log(this.valor2);
   if(this.valor2 != undefined){
     this.flag2 = true;
     /// document.getElementById("valor_input").style.display = 'block';
     console.log('true');
    }else{
      this.flag2 = false;
      //document.getElementById("valor_input").style.display = 'block';
      console.log('false');
    }
  }

  ngOnInit() {
    this.actRoute.params.subscribe((data: any)=>{
      console.log(data);
      this.id = data.id;

      if(this.id!=0){
        this.loadFunc();
      }
    });
  }


  ionViewDidEnter(){
    this.disabledButton = false;
    this.storage.get('storage_xxx').then((res) => {
      console.log(res);
      this.datastorage = res;
    });
  }


  loadFunc(){
    return new Promise(resolve => {
      let body = {
        aksi: 'single_mov',
        id: this.id,
      }
      this.accsPrvds.postData(body, 'mov_api.php').subscribe((res:any)=>{
                this.valor_m = res.result.valor_m;
                this.valor2 = res.result.valor2;
                this.opcao1 = res.result.opcao1;
                this.opcao2 = res.result.opcao2;
                this.dia_m = res.result.dia_m;
                this.detalhes_m = res.result.detalhes_m;
                this.idcliente_m = res.result.idcliente_m;
                this.categoria_m = res.result.categoria_m;
                this.funcionario = res.result.funcionario;
                this.rota = res.result.rota;
      });
    });
  }


  async crudAction(a){
    if(this.valor_m==""){
      this.presentToast('Você precisa digitar o valor');
    }else if(this.idcliente_m=="Sem cliente"){
      this.presentToast('Você precisa selecionar um cliente');
    }else if(this.dia_m==""){
      this.presentToast('Você precisa selecionar a data');
    }else{
      this.disabledButton = true;
      const loader = await this.loadingCtrl.create({
        message: 'Aguarde...',
      });
      loader.present();

      return new Promise(resolve => {
        let body = {
          aksi: 'crud_mov',
          id: this.id,
         // valor_m: this.valor_m,
          valor_m: parseFloat(this.valor_m.replace(',', '.')),
         // valor2: this.valor2,
          valor2: parseFloat(this.valor2.replace(',', '.')),
          opcao1: this.opcao1,
          opcao2: this.opcao2,
          dia_m: this.dia_m,
          detalhes_m: this.detalhes_m,
          idcliente_m: this.idcliente_m,
         categoria_m: this.categoria_m,
          funcionario: this.funcionario,
          rota: this.rota,
          action: a
        }
        this.accsPrvds.postData(body, 'mov_api.php').subscribe((res:any)=>{
          if(res.success==true){
            loader.dismiss();
            this.disabledButton = false;
            this.presentToast(a+res.msg);
            this.router.navigate(['/layout']);
          }else{
            loader.dismiss();
            this.disabledButton = false;
            this.presentAlert(res.msg,a);
          }
        },(err)=>{
          loader.dismiss();
          this.disabledButton = false;
          this.presentAlert('TimeOut',a);
        });
      });
    }
  }

  async presentToast(a){
    const toast = await this.toastCtrl.create({
      message: a,
      duration: 1500,
      position: 'top'
    });
    toast.present();
  }

  async presentAlert(a,b){
   const alert = await this.alertCtrl.create({
     header: a,
     backdropDismiss: false,
     buttons: [
       {
         text:'Fechar',
         cssClass: 'secondary',
         handler: (blah) => {
           console.log('Cancelamento confirmado');
         }
       }, {
         text: 'Ir',
         handler: () => {
           console.log('Okay confirmado');
           this.crudAction(b);
         }
       }
     ]
   });
  }
  linkHome() {
    this.router.navigate(['/mov']);
  }


  loadData(event:any) {
    setTimeout(() => {
      this.loadClientes().then(() => {
        event.target.complete();
      });
    }, 500);
  }

  async loadClientes() {

    return new Promise(resolve => {
      let body = {
        aksi: 'puxar_clientes',
      }
      this.accsPrvds.postData(body, 'clientes_api.php').subscribe((res: any) => {
        for (let datas of res.result) {
          this.clientes.push(datas);
        }
        resolve(true);
      });
    });
  }

  loadData2(event:any) {
    setTimeout(() => {
      this.loadFuncionario().then(() => {
        event.target.complete();
      });
    }, 500);
  }

  async loadFuncionario() {

    return new Promise(resolve => {
      let body = {
        aksi: 'puxar_func',
      }
      this.accsPrvds.postData(body, 'funcionarios_api.php').subscribe((res: any) => {
        for (let datas of res.result) {
          this.funcionarios.push(datas);
        }
        resolve(true);
      });
    });
  }

  loadData3(event:any) {
    setTimeout(() => {
      this.loadCat().then(() => {
        event.target.complete();
      });
    }, 500);
  }

  async loadCat() {

    return new Promise(resolve => {
      let body = {
        aksi: 'puxar_cat',
      }
      this.accsPrvds.postData(body, 'categorias_api.php').subscribe((res: any) => {
        for (let datas of res.result) {
          this.categorias.push(datas);
        }
        resolve(true);
      });
    });
  }

  onChange($event){
    this.idcliente_m = $event.target.value;
    }

    async prosesLogout() {
      this.storage.clear();
      this.NavCtrl.navigateRoot(['/intro']);
      const toast = await this.toastCtrl.create({
        message: 'Você saiu',
        duration: 1500
      });
      toast.present();
    }
}
