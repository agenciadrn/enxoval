import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MovPage } from './mov.page';

const routes: Routes = [
  {
    path: '',
    component: MovPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MovPageRoutingModule {}
