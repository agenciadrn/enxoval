import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MovPageRoutingModule } from './mov-routing.module';

import { MovPage } from './mov.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MovPageRoutingModule
  ],
  declarations: [MovPage]
})
export class MovPageModule {}
