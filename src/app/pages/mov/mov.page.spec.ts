import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MovPage } from './mov.page';

describe('MovPage', () => {
  let component: MovPage;
  let fixture: ComponentFixture<MovPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MovPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
