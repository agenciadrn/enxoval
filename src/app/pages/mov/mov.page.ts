import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastController, LoadingController, AlertController, NavController, Platform } from '@ionic/angular';
import { AccessProviders } from '../../providers/access-providers';
import { Storage } from '@ionic/storage';
import { GlobalService } from '../../providers/global.service';

@Component({
  selector: 'app-mov',
  templateUrl: './mov.page.html',
  styleUrls: ['./mov.page.scss'],
})
export class MovPage implements OnInit {
  datastorage: any;
  name: string;

  movs: any = [];
  limit: number = 13;
  start: number = 0;
  agora: any = new Date;
  dia: number = this.agora.getDate();
  sist: number = this.agora.getMonth();
  meses: any = ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'];
  mesNome: any = this.meses[this.agora.getMonth()];
  mes: number = this.sist + 1;
  ano: number = this.agora.getFullYear();
  semana: number = this.agora.getDay();
  filtro: any;

  constructor(
    private router: Router,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    public platform: Platform,
    private accsPrvds: AccessProviders,
    private storage: Storage,
    public NavCtrl: NavController,
    public global: GlobalService
  ) {
    this.platform.ready().then(() => {
      this.loadingCtrl.create({
        message: 'Carregando...'
      }).then((loadingElement) => {
        loadingElement.present();
        var ref = this;
        setTimeout(function () {
          ref.loadingCtrl.dismiss();
        }, 2000)
      })
    })


  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.storage.get('storage_xxx').then((res) => {
      console.log(res);
      this.datastorage = res;
    });

    this.start = 0;
    this.movs = [];
    this.loadMov();
  }

  async doRefresh(event) {
    const loader = await this.loadingCtrl.create({
      message: 'Aguarde...',
    });
    loader.present();

    this.ionViewDidEnter();
    event.target.complete();

    loader.dismiss();
  }

  loadData(event: any) {
    this.start += this.limit;
    setTimeout(() => {
      this.loadMov().then(() => {
        event.target.complete();
      });
    }, 500);
  }

  async loadMov() {

    return new Promise(resolve => {
      let body = {
        aksi: 'load_mov',
        start: this.start,
        limit: this.limit,
        dia: this.dia,
        mes: this.mes,
        ano: this.ano,
        fitro: this.filtro
      }
      this.accsPrvds.postData(body, 'mov_api.php').subscribe((res: any) => {
        for (let datas of res.result) {
          this.movs.push(datas);
        }
        resolve(true);
      });
    });
  }

  async delData(a) {

    return new Promise(resolve => {
      let body = {
        aksi: 'del_mov',
        id: a
      }
      this.accsPrvds.postData(body, 'mov_api.php').subscribe((res: any) => {
        if (res.success == true) {
          this.presentToast('Deletado com sucesso!');
          this.ionViewDidEnter();
        } else {
          this.presentToast('Erro ao deletar')
        }
      });
    });

  }

  async presentToast(a) {
    const toast = await this.toastCtrl.create({
      message: a,
      duration: 1500
    });
    toast.present();
  }


  openCrud(a) {
    this.router.navigate(['/crud-mov/' + a]);
  }

  linkHome() {
    this.router.navigate(['/home']);
  }


  
  linkHoje(){
    this.router.navigate(['/buscar/0']);
  }
  linkSemana(){
    this.router.navigate(['/buscar/7']);
  }
  linkMes(){
    this.router.navigate(['/buscar/30']);
  }
  linkAno(){
    this.router.navigate(['/buscar/360']);
  }


}
