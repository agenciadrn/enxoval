import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import { AccessProviders } from '../../providers/access-providers';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  your_name: string = "";
  gender: string = "";
  date_birth: string = "";
  email_address: string = "";
  password: string = "";
  confirm_pass: string = "";

  disabledButton;

  constructor(
    private router: Router,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private accsPrvds: AccessProviders
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.disabledButton = false;
  }

  async tryRegister(){
    if(this.your_name==""){
      this.presentToast('Você precisa digitar o nome');
    }else if(this.gender==""){
      this.presentToast('Você precisa preencher com o sexo');
    }else if(this.date_birth==""){
      this.presentToast('Você precisa preencher seu nascimento');
    }else if(this.email_address==""){
      this.presentToast('Você precisa preencher seu e-mail');
    }else if(this.password==""){
      this.presentToast('Você precisa preencher a senha');
    }else if(this.confirm_pass!=this.password){
      this.presentToast('Opa! As senhas estão erradas');
    }else{
      this.disabledButton = true;
      const loader = await this.loadingCtrl.create({
        message: 'Aguarde...',
      });
      loader.present();

      return new Promise(resolve => {
        let body = {
          aksi: 'proses_register',
          your_name: this.your_name,
          gender: this.gender,
          date_birth: this.date_birth,
          email_address: this.email_address,
          password: this.password
        }
        this.accsPrvds.postData(body, 'proses_api.php').subscribe((res:any)=>{
          if(res.success==true){
            loader.dismiss();
            this.disabledButton = false;
            this.presentToast(res.msg);
            this.router.navigate(['/home']);
          }else{
            loader.dismiss();
            this.disabledButton = false;
            this.presentToast(res.msg);
          }
        },(err)=>{
          loader.dismiss();
          this.disabledButton = false;
          this.presentAlert('TimeOut');
        });
      });
    }
  }

  async presentToast(a){
    const toast = await this.toastCtrl.create({
      message: a,
      duration: 1500,
      position: 'top'
    });
    toast.present();
  }

  async presentAlert(a){
   const alert = await this.alertCtrl.create({
     header: a,
     backdropDismiss: false,
     buttons: [
       {
         text:'Fechar',
         cssClass: 'secondary',
         handler: (blah) => {
           console.log('Cancelamento confirmado');
         }
       }, {
         text: 'Ir',
         handler: () => {
           console.log('Okay confirmado');
           this.tryRegister();
         }
       }
     ]
   });
  }

  linkHome() {
    this.router.navigate(['/home']);
  }

}
