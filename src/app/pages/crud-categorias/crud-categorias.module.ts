import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LottieAnimationViewModule } from 'ng-lottie';
import { IonicModule } from '@ionic/angular';

import { CrudCategoriasPageRoutingModule } from './crud-categorias-routing.module';

import { CrudCategoriasPage } from './crud-categorias.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CrudCategoriasPageRoutingModule,
    LottieAnimationViewModule
  ],
  declarations: [CrudCategoriasPage]
})
export class CrudCategoriasPageModule {}
