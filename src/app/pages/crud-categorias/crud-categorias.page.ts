import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import { AccessProviders } from '../../providers/access-providers';
import { GlobalService } from '../../providers/global.service';
import { LottieAnimationViewModule } from 'ng-lottie';

@Component({
  selector: 'app-crud-categorias',
  templateUrl: './crud-categorias.page.html',
  styleUrls: ['./crud-categorias.page.scss'],
})
export class CrudCategoriasPage implements OnInit {

  
  id: number;
  lottieConfig: any;
  nome_c: string = "";
  

  disabledButton;

  constructor(
    private router: Router,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private accsPrvds: AccessProviders,
    private actRoute: ActivatedRoute,
    public global: GlobalService
  ) { 
    LottieAnimationViewModule.forRoot();
 
    this.lottieConfig = {
      path: 'assets/img/categorias.json',
      autoplay: true,
      loop: true
    }
  }

  ngOnInit() {
    this.actRoute.params.subscribe((data: any)=>{
      console.log(data);
      this.id = data.id;

      if(this.id!=0){
        this.loadFunc();
      }
    });
  }

  ionViewDidEnter(){
    this.disabledButton = false;
  }


  loadFunc(){
    return new Promise(resolve => {
      let body = {
        aksi: 'single_cat',
        id: this.id,
      }
      this.accsPrvds.postData(body, 'categorias_api.php').subscribe((res:any)=>{
                this.nome_c = res.result.nome_c;
      });
    });
  }


  async crudAction(a){
    if(this.nome_c==""){
      this.presentToast('Você precisa digitar a categoria');
    }else{
      this.disabledButton = true;
      const loader = await this.loadingCtrl.create({
        message: 'Aguarde...',
      });
      loader.present();

      return new Promise(resolve => {
        let body = {
          aksi: 'crud_cat',
          id: this.id,
          nome_c: this.nome_c,
          action: a
        }
        this.accsPrvds.postData(body, 'categorias_api.php').subscribe((res:any)=>{
          if(res.success==true){
            loader.dismiss();
            this.disabledButton = false;
            this.presentToast(a+res.msg);
            this.router.navigate(['/categorias']);
          }else{
            loader.dismiss();
            this.disabledButton = false;
            this.presentAlert(res.msg,a);
          }
        },(err)=>{
          loader.dismiss();
          this.disabledButton = false;
          this.presentAlert('TimeOut',a);
        });
      });
    }
  }

  async presentToast(a){
    const toast = await this.toastCtrl.create({
      message: a,
      duration: 1500,
      position: 'top'
    });
    toast.present();
  }

  async presentAlert(a,b){
   const alert = await this.alertCtrl.create({
     header: a,
     backdropDismiss: false,
     buttons: [
       {
         text:'Fechar',
         cssClass: 'secondary',
         handler: (blah) => {
           console.log('Cancelamento confirmado');
         }
       }, {
         text: 'Ir',
         handler: () => {
           console.log('Okay confirmado');
           this.crudAction(b);
         }
       }
     ]
   });
  }
  linkHome() {
    this.router.navigate(['/categorias']);
  }

}
