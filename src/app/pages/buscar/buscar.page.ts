import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastController, LoadingController, AlertController, NavController, Platform } from '@ionic/angular';
import { AccessProviders } from '../../providers/access-providers';
import { Storage } from '@ionic/storage';
import { GlobalService } from '../../providers/global.service';

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.page.html',
  styleUrls: ['./buscar.page.scss'],
})
export class BuscarPage implements OnInit {
  datastorage: any;
  name: string;
  id: number;

  movs: any = [];
  limit: number = 13;
  start: number = 0;
  agora: any = new Date;
  dia: number = this.agora.getDate();
  sist: number = this.agora.getMonth();
  meses: any = ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'];
  mesNome: any = this.meses[this.agora.getMonth()];
  mes: number = this.sist + 1;
  ano: number = this.agora.getFullYear();
  semana: number = this.agora.getDay();
  filtro: any;

  constructor(
    private router: Router,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    public platform: Platform,
    private accsPrvds: AccessProviders,
    private storage: Storage,
    public NavCtrl: NavController,
    private actRoute: ActivatedRoute,
    public global: GlobalService
  ) {
    this.platform.ready().then(() => {
      this.loadingCtrl.create({
        message: 'Carregando...'
      }).then((loadingElement) => {
        loadingElement.present();
        var ref = this;
        setTimeout(function () {
          ref.loadingCtrl.dismiss();
        }, 2000)
      })
    })


  }

  ngOnInit() {
    this.actRoute.params.subscribe((data: any)=>{
      console.log(data);
      this.id = data.id;

    });
  }

  ionViewDidEnter() {
    this.storage.get('storage_xxx').then((res) => {
      console.log(res);
      this.datastorage = res;
    });

    this.start = 0;
    this.movs = [];
    this.loadMov();
  }

  async doRefresh(event) {
    const loader = await this.loadingCtrl.create({
      message: 'Aguarde...',
    });
    loader.present();

    this.ionViewDidEnter();
    event.target.complete();

    loader.dismiss();
  }

  loadData(event: any) {
    this.start += this.limit;
    setTimeout(() => {
      this.loadMov().then(() => {
        event.target.complete();
      });
    }, 500);
  }

  async loadMov() {

    return new Promise(resolve => {
      let body = {
        aksi: 'busca_mov',
        start: this.start,
        limit: this.limit,
        id: this.id
      }
      this.accsPrvds.postData(body, 'mov_api.php').subscribe((res: any) => {
        for (let datas of res.result) {
          this.movs.push(datas);
        }
        resolve(true);
      });
    });
  }

  async delData(a) {

    return new Promise(resolve => {
      let body = {
        aksi: 'del_mov',
        id: a
      }
      this.accsPrvds.postData(body, 'mov_api.php').subscribe((res: any) => {
        if (res.success == true) {
          this.presentToast('Deletado com sucesso!');
          this.ionViewDidEnter();
        } else {
          this.presentToast('Erro ao deletar')
        }
      });
    });

  }

  async presentToast(a) {
    const toast = await this.toastCtrl.create({
      message: a,
      duration: 1500
    });
    toast.present();
  }


  openCrud(a) {
    this.router.navigate(['/crud-mov/' + a]);
  }

  linkHome() {
    this.router.navigate(['/home']);
  }

  filtrar(a){
    const on = a.detail.value;
    this.NavCtrl.navigateForward(['/buscar/' + on]);
  }


}
