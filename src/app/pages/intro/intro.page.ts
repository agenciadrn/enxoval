import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastController, LoadingController, AlertController, NavController } from '@ionic/angular';
import { AccessProviders } from '../../providers/access-providers';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss'],
})
export class IntroPage implements OnInit {

  email_address: string = "";
  password: string = "";


  disabledButton;

  constructor(
    private router: Router,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private accsPrvds: AccessProviders,
    private storage: Storage,
    public NavCtrl: NavController
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.disabledButton = false;
  }


  async tryLogin(){
    if(this.email_address==""){
      this.presentToast('Você precisa preencher seu e-mail');
    }else if(this.password==""){
      this.presentToast('Você precisa preencher a senha');
    }else{
      this.disabledButton = true;
      const loader = await this.loadingCtrl.create({
        message: 'Aguarde...',
      });
      loader.present();

      return new Promise(resolve => {
        let body = {
          aksi: 'proses_login',
          email_address: this.email_address,
          password: this.password
        }
        this.accsPrvds.postData(body, 'proses_api.php').subscribe((res:any)=>{
          if(res.success==true){
            loader.dismiss();
            this.disabledButton = false;
            this.presentToast('Login com sucesso!');
            this.storage.set('storage_xxx', res.result);
            this.NavCtrl.navigateRoot(['/layout']);
          }else{
            loader.dismiss();
            this.disabledButton = false;
            this.presentToast('E-mail ou senha estão errados');
          }
        },(err)=>{
          loader.dismiss();
          this.disabledButton = false;
          this.presentToast('TimeOut');
        });
      });
    }
  }

  async presentToast(a){
    const toast = await this.toastCtrl.create({
      message: a,
      duration: 1500,
    });
    toast.present();
  }

  openRegister(){
    this.router.navigate(['/register']);
  }

  

}
