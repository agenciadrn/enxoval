import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BrMaskerModule } from 'br-mask';
import { IonicModule } from '@ionic/angular';
import { LottieAnimationViewModule } from 'ng-lottie';
import { CrudFuncPageRoutingModule } from './crud-func-routing.module';

import { CrudFuncPage } from './crud-func.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BrMaskerModule,
    CrudFuncPageRoutingModule,
    LottieAnimationViewModule
  ],
  declarations: [CrudFuncPage]
})
export class CrudFuncPageModule {}
