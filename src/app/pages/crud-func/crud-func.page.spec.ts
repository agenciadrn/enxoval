import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CrudFuncPage } from './crud-func.page';

describe('CrudFuncPage', () => {
  let component: CrudFuncPage;
  let fixture: ComponentFixture<CrudFuncPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudFuncPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CrudFuncPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
