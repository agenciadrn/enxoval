import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import { AccessProviders } from '../../providers/access-providers';
import { GlobalService } from '../../providers/global.service';
import { LottieAnimationViewModule } from 'ng-lottie';
 
@Component({
  selector: 'app-crud-func',
  templateUrl: './crud-func.page.html',
  styleUrls: ['./crud-func.page.scss'],
})
export class CrudFuncPage implements OnInit {

  id: number;
  lottieConfig: any;
  nome: string = "";
  cpf: string = "";
  cep: string = "";
  bairro: string = "";
  endereco: string = "";
  numero: string = "";
  estado: string = "";
  cidade: string = "";
  

  disabledButton;

  constructor(
    private router: Router,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private accsPrvds: AccessProviders,
    private actRoute: ActivatedRoute,
    public global: GlobalService
  ) {
    LottieAnimationViewModule.forRoot();
 
    this.lottieConfig = {
      path: 'assets/img/funcionarios.json',
      autoplay: true,
      loop: false
    }
   }

  ngOnInit() {
    this.actRoute.params.subscribe((data: any)=>{
      console.log(data);
      this.id = data.id;

      if(this.id!=0){
        this.loadFunc();
      }
    });
  }

  ionViewDidEnter(){
    this.disabledButton = false;
  }


  loadFunc(){
    return new Promise(resolve => {
      let body = {
        aksi: 'single_func',
        id: this.id,
      }
      this.accsPrvds.postData(body, 'funcionarios_api.php').subscribe((res:any)=>{
                this.nome = res.result.nome;
                this.cpf = res.result.cpf;
                this.cep = res.result.cep;
                this.bairro = res.result.bairro;
                this.endereco = res.result.endereco;
                this.numero = res.result.numero;
                this.estado = res.result.estado;
                this.cidade = res.result.cidade;
      });
    });
  }


  async crudAction(a){
    if(this.nome==""){
      this.presentToast('Você precisa digitar o nome');
    }else if(this.cpf==""){
      this.presentToast('Você precisa preencher com o CPF');
    }else{
      this.disabledButton = true;
      const loader = await this.loadingCtrl.create({
        message: 'Aguarde...',
      });
      loader.present();

      return new Promise(resolve => {
        let body = {
          aksi: 'crud_func',
          id: this.id,
          nome: this.nome,
          cpf: this.cpf,
          cep: this.cep,
          bairro: this.bairro,
          endereco: this.endereco,
          numero: this.numero,
          estado: this.estado,
          cidade: this.cidade,
          action: a
        }
        this.accsPrvds.postData(body, 'funcionarios_api.php').subscribe((res:any)=>{
          if(res.success==true){
            loader.dismiss();
            this.disabledButton = false;
            this.presentToast(a+res.msg);
            this.router.navigate(['/funcionarios']);
          }else{
            loader.dismiss();
            this.disabledButton = false;
            this.presentAlert(res.msg,a);
          }
        },(err)=>{
          loader.dismiss();
          this.disabledButton = false;
          this.presentAlert('TimeOut',a);
        });
      });
    }
  }

  async presentToast(a){
    const toast = await this.toastCtrl.create({
      message: a,
      duration: 1500,
      position: 'top'
    });
    toast.present();
  }

  async presentAlert(a,b){
   const alert = await this.alertCtrl.create({
     header: a,
     backdropDismiss: false,
     buttons: [
       {
         text:'Fechar',
         cssClass: 'secondary',
         handler: (blah) => {
           console.log('Cancelamento confirmado');
         }
       }, {
         text: 'Ir',
         handler: () => {
           console.log('Okay confirmado');
           this.crudAction(b);
         }
       }
     ]
   });
  }
  linkHome() {
    this.router.navigate(['/funcionarios']);
  }

}
