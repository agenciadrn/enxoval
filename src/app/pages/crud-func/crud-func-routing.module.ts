import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CrudFuncPage } from './crud-func.page';

const routes: Routes = [
  {
    path: '',
    component: CrudFuncPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CrudFuncPageRoutingModule {}
