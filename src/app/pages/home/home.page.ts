import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastController, LoadingController, AlertController, NavController } from '@ionic/angular';
import { AccessProviders } from '../../providers/access-providers';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  datastorage: any;
  name: string;

  users: any = [];
  limit: number = 13;
  start: number = 0;

  constructor(
    private router: Router,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private accsPrvds: AccessProviders,
    private storage: Storage,
    public NavCtrl: NavController
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.storage.get('storage_xxx').then((res) => {
      console.log(res);
      this.datastorage = res;
      this.name = this.datastorage.your_name;
    });

    this.start = 0;
    this.users = [];
    this.loadUsers();

  }

  async doRefresh(event) {
    const loader = await this.loadingCtrl.create({
      message: 'Por favor...',
    });
    loader.present();

    this.ionViewDidEnter();
    event.target.complete();

    loader.dismiss();
  }

  loadData(event:any) {
    this.start += this.limit;
    setTimeout(() => {
      this.loadUsers().then(() => {
        event.target.complete();
      });
    }, 500);
  }

  async loadUsers() {

    return new Promise(resolve => {
      let body = {
        aksi: 'load_users',
        start: this.start,
        limit: this.limit
      }
      this.accsPrvds.postData(body, 'proses_api.php').subscribe((res: any) => {
        for (let datas of res.result) {
          this.users.push(datas);
        }
        resolve(true);
      });
    });
  }

  async delData(a) {

    return new Promise(resolve => {
      let body = {
        aksi: 'del_users',
        id: a
      }
      this.accsPrvds.postData(body, 'proses_api.php').subscribe((res: any) => {
        if (res.success == true) {
          this.presentToast('Deletado com sucesso!');
          this.ionViewDidEnter();
        } else {
          this.presentToast('Erro ao deletar')
        }
      });
    });

  }

  async presentToast(a) {
    const toast = await this.toastCtrl.create({
      message: a,
      duration: 1500
    });
    toast.present();
  }

  async prosesLogout() {
    this.storage.clear();
    this.NavCtrl.navigateRoot(['/intro']);
    const toast = await this.toastCtrl.create({
      message: 'Você saiu',
      duration: 1500
    });
    toast.present();
  }

  linkMov(){
    this.router.navigate(['/mov']);
  }

  linkAdd(){
    this.router.navigate(['/register']);
  }

  linkFunc(){
    this.router.navigate(['/funcionarios']);
  }

  linkClientes(){
    this.router.navigate(['/clientes']);
  }

  linkCategorias(){
    this.router.navigate(['/categorias']);
  }

  linkRelatorios(){
    this.router.navigate(['/relatorios']);
  }

  linkLayout(){
    this.router.navigate(['/layout']);
  }

}
