import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastController, LoadingController, AlertController, NavController } from '@ionic/angular';
import { AccessProviders } from '../../providers/access-providers';
import { Storage } from '@ionic/storage';
import { GlobalService } from '../../providers/global.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.page.html',
  styleUrls: ['./layout.page.scss'],
})
export class LayoutPage implements OnInit {

  datastorage: any;
  name: string;
 
  users: any = [];
  movs: any = [];
  limit: number = 10;
  start: number = 0;
  agora: any = new Date;
  dia: number = this.agora.getDate();
  sist: number = this.agora.getMonth();
  meses: any = ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'];
  mesNome: any = this.meses[this.agora.getMonth()];
  mes: number = this.sist + 1;
  ano: number = this.agora.getFullYear();
  semana: number = this.agora.getDay();
  filtro: any;
  lottieConfig: any;
  

  constructor(
    private router: Router,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private accsPrvds: AccessProviders,
    private storage: Storage,
    public NavCtrl: NavController,
    public global: GlobalService
  ) {
  }

  ngOnInit() {
    
  }
  ionViewDidEnter() {
    this.storage.get('storage_xxx').then((res) => {
      console.log(res);
      this.datastorage = res;
      this.name = this.datastorage.your_name;
    });

    document.addEventListener("backbutton",function(e) {
      console.log("disable back button")
    }, false);

    this.start = 0;
    this.movs = [];
    this.loadMov();
    this.loadUsers();
    
  }

  

  loadData(event:any) {
    this.start += this.limit;
    setTimeout(() => {
      this.loadUsers().then(() => {
        event.target.complete();
      });
    }, 500);
  }

  async loadUsers() {

    return new Promise(resolve => {
      let body = {
        aksi: 'load_users',
        start: this.start,
        limit: this.limit
      }
      this.accsPrvds.postData(body, 'proses_api.php').subscribe((res: any) => {
        for (let datas of res.result) {
          this.users.push(datas);
        }
        resolve(true);
      });
    });
  }

  async delData(a) {

    return new Promise(resolve => {
      let body = {
        aksi: 'del_users',
        id: a
      }
      this.accsPrvds.postData(body, 'proses_api.php').subscribe((res: any) => {
        if (res.success == true) {
          this.presentToast('Deletado com sucesso!');
          this.ionViewDidEnter();
        } else {
          this.presentToast('Erro ao deletar')
        }
      });
    });

  }

  async presentToast(a) {
    const toast = await this.toastCtrl.create({
      message: a,
      duration: 1500
    });
    toast.present();
  }

  async prosesLogout() {
    this.storage.clear();
    this.NavCtrl.navigateRoot(['/intro']);
    const toast = await this.toastCtrl.create({
      message: 'Você saiu',
      duration: 1500
    });
    toast.present();
  }

  async loadMov() {

    return new Promise(resolve => {
      let body = {
        aksi: 'load_mov',
        start: this.start,
        limit: this.limit,
        dia: this.dia,
        mes: this.mes,
        ano: this.ano,
        fitro: this.filtro
      }
      this.accsPrvds.postData(body, 'mov_api.php').subscribe((res: any) => {
        for (let datas of res.result) {
          this.movs.push(datas);
        }
        resolve(true);
      });
    });
  }

  async delMov(a) {

    return new Promise(resolve => {
      let body = {
        aksi: 'del_mov',
        id: a
      }
      this.accsPrvds.postData(body, 'mov_api.php').subscribe((res: any) => {
        if (res.success == true) {
          this.presentToast('Deletado com sucesso!');
          this.ionViewDidEnter();
        } else {
          this.presentToast('Erro ao deletar')
        }
      });
    });

  }

  openCrud(a) {
    this.NavCtrl.navigateForward(['/crud-mov/' + a]);
  }

  atualizar(){
    this.ionViewDidEnter();
  }
  
  filtrar(a){
    const on = a.detail.value;
    this.NavCtrl.navigateForward(['/buscar/' + on]);
    this.filtro = '';
  }

  async presentAlertConfirm(a) {
    const alert = await this.alertCtrl.create({
      header: 'Deseja realmente deletar?',
      message: 'Confirme abaixo para excluir essa movimentação.',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (a) => {
            this.presentToast('Cancelado')
          }
        }, {
          text: 'Eu confirmo',
          handler: () => {
            this.delMov(a);
          }
        }
      ]
    });
    await alert.present();
  }


}