import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CrudClientesPage } from './crud-clientes.page';

const routes: Routes = [
  {
    path: '',
    component: CrudClientesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CrudClientesPageRoutingModule {}
