import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BrMaskerModule } from 'br-mask';
import { IonicModule } from '@ionic/angular';

import { LottieAnimationViewModule } from 'ng-lottie';
import { CrudClientesPageRoutingModule } from './crud-clientes-routing.module';

import { CrudClientesPage } from './crud-clientes.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BrMaskerModule,
    LottieAnimationViewModule,
    CrudClientesPageRoutingModule
  ],
  declarations: [CrudClientesPage]
})
export class CrudClientesPageModule {}
