import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CrudClientesPage } from './crud-clientes.page';

describe('CrudClientesPage', () => {
  let component: CrudClientesPage;
  let fixture: ComponentFixture<CrudClientesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudClientesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CrudClientesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
