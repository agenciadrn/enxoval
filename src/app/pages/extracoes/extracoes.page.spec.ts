import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ExtracoesPage } from './extracoes.page';

describe('ExtracoesPage', () => {
  let component: ExtracoesPage;
  let fixture: ComponentFixture<ExtracoesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtracoesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ExtracoesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
