import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExtracoesPage } from './extracoes.page';

const routes: Routes = [
  {
    path: '',
    component: ExtracoesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExtracoesPageRoutingModule {}
