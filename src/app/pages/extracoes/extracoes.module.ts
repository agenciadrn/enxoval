import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExtracoesPageRoutingModule } from './extracoes-routing.module';

import { ExtracoesPage } from './extracoes.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExtracoesPageRoutingModule
  ],
  declarations: [ExtracoesPage]
})
export class ExtracoesPageModule {}
