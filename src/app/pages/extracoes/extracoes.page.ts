import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastController, LoadingController, AlertController, NavController, Platform } from '@ionic/angular';
import { AccessProviders } from '../../providers/access-providers';
import { Storage } from '@ionic/storage';
import { GlobalService } from '../../providers/global.service';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { File } from "@ionic-native/file/ngx";
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import {
  FileTransfer,
  FileTransferObject
} from "@ionic-native/file-transfer/ngx";
import { FileOpener } from "@ionic-native/file-opener/ngx";

@Component({
  selector: 'app-extracoes',
  templateUrl: './extracoes.page.html',
  styleUrls: ['./extracoes.page.scss'],
})
export class ExtracoesPage {


 real: any;
  bars: any;
  colorArray: any;
  datastorage: any;
  name: string;
  movs: any = [];
  limit: number = 13;
  start: number = 0;
  agora: any = new Date;
  dia: number = this.agora.getDate();
  sist: number = this.agora.getMonth();
  mesfloat: any = ['01','03','02','04','05','06','07','08','09','10','11','12'];
  meses: any = ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'];
  mesNome: any = this.meses[this.agora.getMonth()];
  mes: number = this.mesfloat[this.agora.getMonth()];
  ano: number = this.agora.getFullYear();
  semana: number = this.agora.getDay();
  mes2: any = this.mes;
  ano2: number = this.ano;
  dia2: number = this.dia;
  semana2: number = this.semana;
  filtro: any;
  id: any;
  extracoes: any;
  loaderToShow: any;
  excel: any;
  

  constructor(
    private router: Router,
    private fileOpener: FileOpener,
    private transfer: FileTransfer,
    private androidPermissions: AndroidPermissions,
    private file: File,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    public platform: Platform,
    private accsPrvds: AccessProviders,
    private storage: Storage,
    public global: GlobalService,
    public NavCtrl: NavController,
    private http: HttpClient
  ) {
    
    this.verExtracoes(Event);
    this.excel = false;
   
  }
  carregando() {
    this.loaderToShow = this.loadingCtrl.create({
      message: 'Carregando...'
    }).then((res) => {
      res.present();
 
      res.onDidDismiss().then((dis) => {
        console.log('Loading dismissed!');
      });
    });
    this.carregado();
  }

  carregado() {
    setTimeout(() => {
      this.loadingCtrl.dismiss();
    }, 2000);
  }

 

  ionViewDidEnter() {
    this.storage.get('storage_xxx').then((res) => {
      console.log(res);
      this.datastorage = res;
    });

    this.start = 0;
    this.movs = [];
    
  }

  loadData(event: any) {
    this.start += this.limit;
    setTimeout(() => {
      this.verExtracoes(event).then(() => {
        event.target.complete();
      });
    }, 500);
  }


  async verExtracoes(id) {
    if(id.detail != undefined){
      this.carregando();
      const periodo = id.detail.value;
        this.extracoes = periodo;
        console.log(id.detail.value);
        this.ionViewDidEnter();
        this.carregado();
      }else{
        this.extracoes = "30";
      }

    return new Promise(resolve => {
      let body = {
        aksi: 'extracoes',
        start: this.start,
        limit: this.limit,
        dia: this.dia,
        mes: this.mes,
        ano: this.ano,
        id: this.extracoes,
        fitro: this.filtro
      }
      this.accsPrvds.postData(body, 'extracoes_api.php').subscribe((res: any) => {
        for (let datas of res.result) {
          this.movs.push(datas);
        }
        resolve(true);
      });
    });
  }

  async delData(a) {

    return new Promise(resolve => {
      let body = {
        aksi: 'del_mov',
        id: a
      }
      this.accsPrvds.postData(body, 'mov_api.php').subscribe((res: any) => {
        if (res.success == true) {
          this.presentToast('Deletado com sucesso!');
          this.ionViewDidEnter();
        } else {
          this.presentToast('Erro ao deletar')
        }
      });
    });

  }

  async presentToast(a) {
    const toast = await this.toastCtrl.create({
      message: a,
      duration: 1500
    });
    toast.present();
  }


  openCrud(a) {
    this.router.navigate(['/crud-mov/' + a]);
  }

  linkHome() {
    this.router.navigate(['/home']);
  }


  
  linkHoje(){
    this.router.navigate(['/buscar/0']);
  }
  linkSemana(){
    this.router.navigate(['/buscar/7']);
  }
  linkMes(){
    this.router.navigate(['/buscar/30']);
  }
  linkAno(){
    this.router.navigate(['/buscar/360']);
  }

  
    downloadXLS(e){
      const on = e.detail.checked;
      
        if (on == true) {
          this.accsPrvds.server + 'relatorio.php' + this.extracoes;
          this.global.download(
            this.accsPrvds.server + 'relatorio.xls',
            'Relatorio'
          );
          console.log(on);
        }else{
          console.log(on);
        }
    }


}
