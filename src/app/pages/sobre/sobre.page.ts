import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastController, LoadingController, AlertController, NavController, Platform } from '@ionic/angular';
import { AccessProviders } from '../../providers/access-providers';
import { LottieAnimationViewModule } from 'ng-lottie';
import { Storage } from '@ionic/storage';
import { GlobalService } from '../../providers/global.service';

@Component({
  selector: 'app-sobre',
  templateUrl: './sobre.page.html',
  styleUrls: ['./sobre.page.scss'],
})
export class SobrePage implements OnInit {

  datastorage: any;
  id: number;
 
  valor_m: string = "";
  valor2: string = "";
  opcao1: string = "";
  opcao2: string = "";
  dia_m: string = "";
  idcliente_m: any;
  detalhes_m: string = "";
  categoria_m: string = "";
  funcionario: string = "";
  rota: string = "";
  
  clientes: any = [];
  funcionarios: any = [];
  categorias: any = [];
  
  flag1 = false;
  flag2 = false;
  
  lottieConfig:any;

  disabledButton;

  constructor(
    private router: Router,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private accsPrvds: AccessProviders,
    public platform: Platform,
    private actRoute: ActivatedRoute,
    public NavCtrl: NavController,
    private storage: Storage,
    public global: GlobalService
  ) { 
    LottieAnimationViewModule.forRoot();
 
    this.lottieConfig = {
      path: 'assets/img/marketing.json',
      autoplay: true,
      loop: true
    }
  }

  ionViewDidEnter(){
    this.disabledButton = false;
    this.storage.get('storage_xxx').then((res) => {
      console.log(res);
      this.datastorage = res;
    });
  }
  
  ngOnInit() {
  }

  async prosesLogout() {
    this.storage.clear();
    this.NavCtrl.navigateRoot(['/intro']);
    const toast = await this.toastCtrl.create({
      message: 'Você saiu',
      duration: 1500
    });
    toast.present();
  }

}
