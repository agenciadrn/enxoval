import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { IonicModule } from '@ionic/angular';
import { Chart } from 'chart.js';
import { RelatoriosPageRoutingModule } from './relatorios-routing.module';
import { ChartsModule } from 'ng2-charts';
import { RelatoriosPage } from './relatorios.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpClientModule,
    ChartsModule,
    RelatoriosPageRoutingModule
  ],
  declarations: [RelatoriosPage]
})
export class RelatoriosPageModule {}
