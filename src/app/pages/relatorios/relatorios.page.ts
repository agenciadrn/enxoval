import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastController, LoadingController, AlertController, NavController, Platform } from '@ionic/angular';
import { AccessProviders } from '../../providers/access-providers';
import { Storage } from '@ionic/storage';
import { GlobalService } from '../../providers/global.service';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { ChartDataSets } from 'chart.js';
import { Color, Label } from 'ng2-charts';


@Component({
  selector: 'app-relatorios',
  templateUrl: './relatorios.page.html',
  styleUrls: ['./relatorios.page.scss'],
})
export class RelatoriosPage {

  chartData: ChartDataSets[] = [
    { data : [], label: 'Receitas' },
    { data : [], label: 'Despesas' }
  ]
  chartLabels: Label[];

  chartOptions = {
    responsive: true,
    title: {
      display: true,
      text: 'Histórico em dias'
    },
    pan: {
      enable: true,
      mode: 'xy'
    },
    zoom: {
      enabled: true,
      mode: 'xy'
    }
  };

 

  chartColors: Color[] = [
    {
      backgroundColor: '#32CD32'
    },
    {
      backgroundColor: '#ff0000'
    }
  ];
  chartType = 'bar';
  showLegend = true;

  stock = '';


 // @ViewChild('barChart') barChart;
 real: any;
  bars: any;
  colorArray: any;
  datastorage: any;
  name: string;
  movs: any = [];
  limit: number = 13;
  start: number = 0;
  agora: any = new Date;
  dia: number = this.agora.getDate();
  sist: number = this.agora.getMonth();
  mesfloat: any = ['01','03','02','04','05','06','07','08','09','10','11','12'];
  meses: any = ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'];
  mesNome: any = this.meses[this.agora.getMonth()];
  mes: number = this.mesfloat[this.agora.getMonth()];
  ano: number = this.agora.getFullYear();
  semana: number = this.agora.getDay();
  mes2: any = this.mes;
  ano2: number = this.ano;
  dia2: number = this.dia;
  semana2: number = this.semana;
  filtro: any;
  

  constructor(
    private router: Router,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    public platform: Platform,
    private accsPrvds: AccessProviders,
    private storage: Storage,
    public global: GlobalService,
    public NavCtrl: NavController,
    private http: HttpClient
  ) {
    

    
    this.verGraficos(Event);
   
    
  }


  ionViewDidEnter() {
    this.storage.get('storage_xxx').then((res) => {
      console.log(res);
      this.datastorage = res;
    });

    this.start = 0;
    this.movs = [];
    
  }

  
  async verGraficos(m) {
    if(m.detail != undefined){
      const mes = m.detail.value;
        this.mes2 = mes;
        console.log(m.detail.value);
      }else if(this.mes2 == 2019){
        const ano = m.detail.value;
        this.filtro = '2019';
        this.ano2 = 2019;
        console.log(m.detail);
      }else{
        this.mes2 = this.mes;
        
      }

    return new Promise(resolve => {
      let body = {
        aksi: 'graficos',
        start: this.start,
        limit: this.limit,
        dia2: this.dia2,
        mes2: this.mes2,
        ano2: this.ano2,
        fitro: this.filtro
      }
      this.accsPrvds.postData(body, 'mov_api.php').subscribe((res: any) => {
        const history = res['valor_m'];

        this.chartLabels = [];
        this.chartData[0].data = [];
        for (let data of res.result) {
          this.movs.push(data);
          this.chartLabels.push(data.dias);
          this.chartData[0].data.push(data['valores']);
          this.chartData[1].data.push(data['valor2']);
          console.log(this.chartData[1]);
        }
        resolve(true);
      });
    });
  }

  

  async delData(a) {

    return new Promise(resolve => {
      let body = {
        aksi: 'del_mov',
        id: a
      }
      this.accsPrvds.postData(body, 'mov_api.php').subscribe((res: any) => {
        if (res.success == true) {
          this.presentToast('Deletado com sucesso!');
          this.ionViewDidEnter();
        } else {
          this.presentToast('Erro ao deletar')
        }
      });
    });

  }

  async presentToast(a) {
    const toast = await this.toastCtrl.create({
      message: a,
      duration: 1500
    });
    toast.present();
  }


  openCrud(a) {
    this.router.navigate(['/crud-mov/' + a]);
  }

  linkHome() {
    this.router.navigate(['/home']);
  }


  
  linkHoje(){
    this.router.navigate(['/buscar/0']);
  }
  linkSemana(){
    this.router.navigate(['/buscar/7']);
  }
  linkMes(){
    this.router.navigate(['/buscar/30']);
  }
  linkAno(){
    this.router.navigate(['/buscar/360']);
  }

  


    typeChanged(e){
      const on = e.detail.value;
      this.chartType = on;
    }
    /*
    typeChanged(e){
      const on = e.detail.checked;
      this.chartType = on ? 'line' : 'bar';
    }

    */
}
