import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LottieAnimationViewModule } from 'ng-lottie';
import { IonicModule } from '@ionic/angular';

import { ControlePageRoutingModule } from './controle-routing.module';

import { ControlePage } from './controle.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ControlePageRoutingModule,
    LottieAnimationViewModule
  ],
  declarations: [ControlePage]
})
export class ControlePageModule {}
